/*
 * Copyright (C) 2013 Frederic Leroy <fredo@starox.org>
 *
 * Based on Kirkwood support:
 * (C) Copyright 2009
 * Marvell Semiconductor <www.marvell.com>
 * Written-by: Prafulla Wadaskar <prafulla@marvell.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */

#include <common.h>
#include <command.h>
#include <asm/arch/cpu.h>
#include <asm/arch/kirkwood.h>
#include <asm/arch/mpp.h>
#include <asm/arch/gpio.h>

#include "cloudbox.h"
#include "../common/common.h"

DECLARE_GLOBAL_DATA_PTR;

int board_early_init_f(void)
{
	/* Gpio configuration */
	kw_config_gpio(CLOUDBOX_OE_VAL_LOW, CLOUDBOX_OE_VAL_HIGH,
			CLOUDBOX_OE_LOW, CLOUDBOX_OE_HIGH);

	/* Multi-Purpose Pins Functionality configuration */
	static const u32 kwmpp_config[] = {
		MPP0_SPI_SCn,
		MPP1_SPI_MOSI,
		MPP2_SPI_SCK,
		MPP3_SPI_MISO,
		MPP4_GPIO, /* hard disk power */
		MPP5_GPO,
		MPP6_SYSRST_OUTn,
		MPP7_GPO,
		MPP8_TW_SDA,
		MPP9_TW_SCK,
		MPP10_UART0_TXD,
		MPP11_UART0_RXD,
		MPP12_GPO,
		MPP14_GPIO, /* LED red control */
		MPP15_SATA0_ACTn, /* LED blue control */
		MPP16_GPIO, /* power push buton */
		MPP17_GPIO, /* board power off */
		MPP20_GPIO, /* Ethernet PHY interrupt (WoL) */
		MPP28_GPIO, /* board revision (LSB) */
		MPP29_GPIO, /* board revision */
		MPP30_GPIO, /* board revision */
		MPP31_GPIO, /* board revision */
		MPP32_GPIO, /* board revision (MSB) */
		0
	};
	kirkwood_mpp_conf(kwmpp_config, NULL);

	return 0;
}

int board_init(void)
{
	/* Nothing to do with fdt */
	return 0;
}

#if defined(CONFIG_MISC_INIT_R)
int misc_init_r(void)
{
       return 0;
}
#endif /* CONFIG_MISC_INIT_R */

#if defined(CONFIG_CMD_NET) && defined(CONFIG_RESET_PHY_R)
/* Configure and initialize PHY */
void reset_phy(void)
{
	mv_phy_88e1318_init("egiga0", 0);
}
#endif

#if defined(CONFIG_KIRKWOOD_GPIO)
/* Return GPIO button status */
static int
do_read_button(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	return kw_gpio_get_value(CLOUDBOX_GPIO_BUTTON);
}

U_BOOT_CMD(button, 1, 1, do_read_button,
	   "Return GPIO button status 0=off 1=on", "");
#endif
