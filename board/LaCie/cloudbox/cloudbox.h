/*
 * Copyright (C) 2013 Frederic Leroy <fredo@starox.org>
 *
 * Based on LaCie u-boot sources
 *
 * Based on Kirkwood support:
 * (C) Copyright 2009
 * Marvell Semiconductor <www.marvell.com>
 * Written-by: Prafulla Wadaskar <prafulla@marvell.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */

#ifndef CLOUDBOX_H
#define CLOUDBOX_H

/* GPIO configuration */
#define CLOUDBOX_OE_LOW		0xF0310000
#define CLOUDBOX_OE_HIGH		0x00000001
#define CLOUDBOX_OE_VAL_LOW		0x00000010
#define CLOUDBOX_OE_VAL_HIGH		0x00000000

#define CLOUDBOX_GPIO_BUTTON         16

#endif /* CLOUDBOX_H */
